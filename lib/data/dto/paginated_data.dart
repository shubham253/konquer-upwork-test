import 'package:freezed_annotation/freezed_annotation.dart';

part 'paginated_data.freezed.dart';

@freezed
class PaginatedData<T> with _$PaginatedData<T> {
  const factory PaginatedData({
    String? next,
    String? previous,
    int? count,
    List<T>? items,
    int? page,
  }) = _PaginatedData<T>;

  factory PaginatedData.fromJson(
      Map<String, dynamic> json, T Function(Map<String, dynamic>) fromJsonT) {
    final items = (json['results'] as List<dynamic>?)
        ?.map((e) => fromJsonT(e as Map<String, dynamic>))
        .toList();
    return PaginatedData<T>(
      next: json['next'] as String?,
      previous: json['previous'] as String?,
      count: json['count'] as int?,
      page: json['page'] as int?,
      items: items,
    );
  }
}
