// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'fighter_info.freezed.dart';

part 'fighter_info.g.dart';

@freezed
class FighterInfo with _$FighterInfo {
  const FighterInfo._();

  const factory FighterInfo({
    @JsonKey(name: 'first_name') @Default('NA') String firstName,
    @JsonKey(name: 'last_name') @Default('NA') String lastName,
    @JsonKey(name: 'usa_boxing_id') String? usaBoxingId,
    @Default('NA') String gender,
    @Default('0') String weight,
    @JsonKey(name: 'date_of_birth') DateTime? dateOfBirth,
    @JsonKey(name: 'number_of_fights') int? numberOfFights,
    @JsonKey(name: 'team_id') int? teamId,
    @JsonKey(name: 'gym_name') String? gymName,
    @JsonKey(name: 'picture_thumb') String? pictureThumb,
    @JsonKey(name: 'is_boxer') bool? isBoxer,
    @JsonKey(name: 'is_coach') bool? isCoach,
    @JsonKey(name: 'is_official') bool? isOfficial,
    @JsonKey(name: 'is_matchmaker') bool? isMatchmaker,
    @JsonKey(name: 'is_managed_account') bool? isManagedAccount,
    int? id,
    @Default(0) int age,
    @JsonKey(name: 'gym_location_full') GymLocation? gymLocation,
  }) = _FighterInfo;

  factory FighterInfo.fromJson(Map<String, dynamic> json) =>
      _$FighterInfoFromJson(json);
}

@freezed
class GymLocation with _$GymLocation {
  const GymLocation._(); // Add this line for private constructor

  const factory GymLocation({
    String? locality,
    String? state,
    @JsonKey(name: 'state_code') String? stateCode,
    String? country,
    @JsonKey(name: 'country_code') String? countryCode,
    String? raw,
  }) = _GymLocation;

  factory GymLocation.fromJson(Map<String, dynamic> json) =>
      _$GymLocationFromJson(json);
}
