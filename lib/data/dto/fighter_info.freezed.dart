// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'fighter_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

FighterInfo _$FighterInfoFromJson(Map<String, dynamic> json) {
  return _FighterInfo.fromJson(json);
}

/// @nodoc
mixin _$FighterInfo {
  @JsonKey(name: 'first_name')
  String get firstName => throw _privateConstructorUsedError;
  @JsonKey(name: 'last_name')
  String get lastName => throw _privateConstructorUsedError;
  @JsonKey(name: 'usa_boxing_id')
  String? get usaBoxingId => throw _privateConstructorUsedError;
  String get gender => throw _privateConstructorUsedError;
  String get weight => throw _privateConstructorUsedError;
  @JsonKey(name: 'date_of_birth')
  DateTime? get dateOfBirth => throw _privateConstructorUsedError;
  @JsonKey(name: 'number_of_fights')
  int? get numberOfFights => throw _privateConstructorUsedError;
  @JsonKey(name: 'team_id')
  int? get teamId => throw _privateConstructorUsedError;
  @JsonKey(name: 'gym_name')
  String? get gymName => throw _privateConstructorUsedError;
  @JsonKey(name: 'picture_thumb')
  String? get pictureThumb => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_boxer')
  bool? get isBoxer => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_coach')
  bool? get isCoach => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_official')
  bool? get isOfficial => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_matchmaker')
  bool? get isMatchmaker => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_managed_account')
  bool? get isManagedAccount => throw _privateConstructorUsedError;
  int? get id => throw _privateConstructorUsedError;
  int get age => throw _privateConstructorUsedError;
  @JsonKey(name: 'gym_location_full')
  GymLocation? get gymLocation => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $FighterInfoCopyWith<FighterInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FighterInfoCopyWith<$Res> {
  factory $FighterInfoCopyWith(
          FighterInfo value, $Res Function(FighterInfo) then) =
      _$FighterInfoCopyWithImpl<$Res, FighterInfo>;
  @useResult
  $Res call(
      {@JsonKey(name: 'first_name') String firstName,
      @JsonKey(name: 'last_name') String lastName,
      @JsonKey(name: 'usa_boxing_id') String? usaBoxingId,
      String gender,
      String weight,
      @JsonKey(name: 'date_of_birth') DateTime? dateOfBirth,
      @JsonKey(name: 'number_of_fights') int? numberOfFights,
      @JsonKey(name: 'team_id') int? teamId,
      @JsonKey(name: 'gym_name') String? gymName,
      @JsonKey(name: 'picture_thumb') String? pictureThumb,
      @JsonKey(name: 'is_boxer') bool? isBoxer,
      @JsonKey(name: 'is_coach') bool? isCoach,
      @JsonKey(name: 'is_official') bool? isOfficial,
      @JsonKey(name: 'is_matchmaker') bool? isMatchmaker,
      @JsonKey(name: 'is_managed_account') bool? isManagedAccount,
      int? id,
      int age,
      @JsonKey(name: 'gym_location_full') GymLocation? gymLocation});

  $GymLocationCopyWith<$Res>? get gymLocation;
}

/// @nodoc
class _$FighterInfoCopyWithImpl<$Res, $Val extends FighterInfo>
    implements $FighterInfoCopyWith<$Res> {
  _$FighterInfoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? firstName = null,
    Object? lastName = null,
    Object? usaBoxingId = freezed,
    Object? gender = null,
    Object? weight = null,
    Object? dateOfBirth = freezed,
    Object? numberOfFights = freezed,
    Object? teamId = freezed,
    Object? gymName = freezed,
    Object? pictureThumb = freezed,
    Object? isBoxer = freezed,
    Object? isCoach = freezed,
    Object? isOfficial = freezed,
    Object? isMatchmaker = freezed,
    Object? isManagedAccount = freezed,
    Object? id = freezed,
    Object? age = null,
    Object? gymLocation = freezed,
  }) {
    return _then(_value.copyWith(
      firstName: null == firstName
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: null == lastName
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      usaBoxingId: freezed == usaBoxingId
          ? _value.usaBoxingId
          : usaBoxingId // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      weight: null == weight
          ? _value.weight
          : weight // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: freezed == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      numberOfFights: freezed == numberOfFights
          ? _value.numberOfFights
          : numberOfFights // ignore: cast_nullable_to_non_nullable
              as int?,
      teamId: freezed == teamId
          ? _value.teamId
          : teamId // ignore: cast_nullable_to_non_nullable
              as int?,
      gymName: freezed == gymName
          ? _value.gymName
          : gymName // ignore: cast_nullable_to_non_nullable
              as String?,
      pictureThumb: freezed == pictureThumb
          ? _value.pictureThumb
          : pictureThumb // ignore: cast_nullable_to_non_nullable
              as String?,
      isBoxer: freezed == isBoxer
          ? _value.isBoxer
          : isBoxer // ignore: cast_nullable_to_non_nullable
              as bool?,
      isCoach: freezed == isCoach
          ? _value.isCoach
          : isCoach // ignore: cast_nullable_to_non_nullable
              as bool?,
      isOfficial: freezed == isOfficial
          ? _value.isOfficial
          : isOfficial // ignore: cast_nullable_to_non_nullable
              as bool?,
      isMatchmaker: freezed == isMatchmaker
          ? _value.isMatchmaker
          : isMatchmaker // ignore: cast_nullable_to_non_nullable
              as bool?,
      isManagedAccount: freezed == isManagedAccount
          ? _value.isManagedAccount
          : isManagedAccount // ignore: cast_nullable_to_non_nullable
              as bool?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      age: null == age
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
      gymLocation: freezed == gymLocation
          ? _value.gymLocation
          : gymLocation // ignore: cast_nullable_to_non_nullable
              as GymLocation?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $GymLocationCopyWith<$Res>? get gymLocation {
    if (_value.gymLocation == null) {
      return null;
    }

    return $GymLocationCopyWith<$Res>(_value.gymLocation!, (value) {
      return _then(_value.copyWith(gymLocation: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$FighterInfoImplCopyWith<$Res>
    implements $FighterInfoCopyWith<$Res> {
  factory _$$FighterInfoImplCopyWith(
          _$FighterInfoImpl value, $Res Function(_$FighterInfoImpl) then) =
      __$$FighterInfoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'first_name') String firstName,
      @JsonKey(name: 'last_name') String lastName,
      @JsonKey(name: 'usa_boxing_id') String? usaBoxingId,
      String gender,
      String weight,
      @JsonKey(name: 'date_of_birth') DateTime? dateOfBirth,
      @JsonKey(name: 'number_of_fights') int? numberOfFights,
      @JsonKey(name: 'team_id') int? teamId,
      @JsonKey(name: 'gym_name') String? gymName,
      @JsonKey(name: 'picture_thumb') String? pictureThumb,
      @JsonKey(name: 'is_boxer') bool? isBoxer,
      @JsonKey(name: 'is_coach') bool? isCoach,
      @JsonKey(name: 'is_official') bool? isOfficial,
      @JsonKey(name: 'is_matchmaker') bool? isMatchmaker,
      @JsonKey(name: 'is_managed_account') bool? isManagedAccount,
      int? id,
      int age,
      @JsonKey(name: 'gym_location_full') GymLocation? gymLocation});

  @override
  $GymLocationCopyWith<$Res>? get gymLocation;
}

/// @nodoc
class __$$FighterInfoImplCopyWithImpl<$Res>
    extends _$FighterInfoCopyWithImpl<$Res, _$FighterInfoImpl>
    implements _$$FighterInfoImplCopyWith<$Res> {
  __$$FighterInfoImplCopyWithImpl(
      _$FighterInfoImpl _value, $Res Function(_$FighterInfoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? firstName = null,
    Object? lastName = null,
    Object? usaBoxingId = freezed,
    Object? gender = null,
    Object? weight = null,
    Object? dateOfBirth = freezed,
    Object? numberOfFights = freezed,
    Object? teamId = freezed,
    Object? gymName = freezed,
    Object? pictureThumb = freezed,
    Object? isBoxer = freezed,
    Object? isCoach = freezed,
    Object? isOfficial = freezed,
    Object? isMatchmaker = freezed,
    Object? isManagedAccount = freezed,
    Object? id = freezed,
    Object? age = null,
    Object? gymLocation = freezed,
  }) {
    return _then(_$FighterInfoImpl(
      firstName: null == firstName
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      lastName: null == lastName
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String,
      usaBoxingId: freezed == usaBoxingId
          ? _value.usaBoxingId
          : usaBoxingId // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      weight: null == weight
          ? _value.weight
          : weight // ignore: cast_nullable_to_non_nullable
              as String,
      dateOfBirth: freezed == dateOfBirth
          ? _value.dateOfBirth
          : dateOfBirth // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      numberOfFights: freezed == numberOfFights
          ? _value.numberOfFights
          : numberOfFights // ignore: cast_nullable_to_non_nullable
              as int?,
      teamId: freezed == teamId
          ? _value.teamId
          : teamId // ignore: cast_nullable_to_non_nullable
              as int?,
      gymName: freezed == gymName
          ? _value.gymName
          : gymName // ignore: cast_nullable_to_non_nullable
              as String?,
      pictureThumb: freezed == pictureThumb
          ? _value.pictureThumb
          : pictureThumb // ignore: cast_nullable_to_non_nullable
              as String?,
      isBoxer: freezed == isBoxer
          ? _value.isBoxer
          : isBoxer // ignore: cast_nullable_to_non_nullable
              as bool?,
      isCoach: freezed == isCoach
          ? _value.isCoach
          : isCoach // ignore: cast_nullable_to_non_nullable
              as bool?,
      isOfficial: freezed == isOfficial
          ? _value.isOfficial
          : isOfficial // ignore: cast_nullable_to_non_nullable
              as bool?,
      isMatchmaker: freezed == isMatchmaker
          ? _value.isMatchmaker
          : isMatchmaker // ignore: cast_nullable_to_non_nullable
              as bool?,
      isManagedAccount: freezed == isManagedAccount
          ? _value.isManagedAccount
          : isManagedAccount // ignore: cast_nullable_to_non_nullable
              as bool?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      age: null == age
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
      gymLocation: freezed == gymLocation
          ? _value.gymLocation
          : gymLocation // ignore: cast_nullable_to_non_nullable
              as GymLocation?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$FighterInfoImpl extends _FighterInfo {
  const _$FighterInfoImpl(
      {@JsonKey(name: 'first_name') this.firstName = 'NA',
      @JsonKey(name: 'last_name') this.lastName = 'NA',
      @JsonKey(name: 'usa_boxing_id') this.usaBoxingId,
      this.gender = 'NA',
      this.weight = '0',
      @JsonKey(name: 'date_of_birth') this.dateOfBirth,
      @JsonKey(name: 'number_of_fights') this.numberOfFights,
      @JsonKey(name: 'team_id') this.teamId,
      @JsonKey(name: 'gym_name') this.gymName,
      @JsonKey(name: 'picture_thumb') this.pictureThumb,
      @JsonKey(name: 'is_boxer') this.isBoxer,
      @JsonKey(name: 'is_coach') this.isCoach,
      @JsonKey(name: 'is_official') this.isOfficial,
      @JsonKey(name: 'is_matchmaker') this.isMatchmaker,
      @JsonKey(name: 'is_managed_account') this.isManagedAccount,
      this.id,
      this.age = 0,
      @JsonKey(name: 'gym_location_full') this.gymLocation})
      : super._();

  factory _$FighterInfoImpl.fromJson(Map<String, dynamic> json) =>
      _$$FighterInfoImplFromJson(json);

  @override
  @JsonKey(name: 'first_name')
  final String firstName;
  @override
  @JsonKey(name: 'last_name')
  final String lastName;
  @override
  @JsonKey(name: 'usa_boxing_id')
  final String? usaBoxingId;
  @override
  @JsonKey()
  final String gender;
  @override
  @JsonKey()
  final String weight;
  @override
  @JsonKey(name: 'date_of_birth')
  final DateTime? dateOfBirth;
  @override
  @JsonKey(name: 'number_of_fights')
  final int? numberOfFights;
  @override
  @JsonKey(name: 'team_id')
  final int? teamId;
  @override
  @JsonKey(name: 'gym_name')
  final String? gymName;
  @override
  @JsonKey(name: 'picture_thumb')
  final String? pictureThumb;
  @override
  @JsonKey(name: 'is_boxer')
  final bool? isBoxer;
  @override
  @JsonKey(name: 'is_coach')
  final bool? isCoach;
  @override
  @JsonKey(name: 'is_official')
  final bool? isOfficial;
  @override
  @JsonKey(name: 'is_matchmaker')
  final bool? isMatchmaker;
  @override
  @JsonKey(name: 'is_managed_account')
  final bool? isManagedAccount;
  @override
  final int? id;
  @override
  @JsonKey()
  final int age;
  @override
  @JsonKey(name: 'gym_location_full')
  final GymLocation? gymLocation;

  @override
  String toString() {
    return 'FighterInfo(firstName: $firstName, lastName: $lastName, usaBoxingId: $usaBoxingId, gender: $gender, weight: $weight, dateOfBirth: $dateOfBirth, numberOfFights: $numberOfFights, teamId: $teamId, gymName: $gymName, pictureThumb: $pictureThumb, isBoxer: $isBoxer, isCoach: $isCoach, isOfficial: $isOfficial, isMatchmaker: $isMatchmaker, isManagedAccount: $isManagedAccount, id: $id, age: $age, gymLocation: $gymLocation)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FighterInfoImpl &&
            (identical(other.firstName, firstName) ||
                other.firstName == firstName) &&
            (identical(other.lastName, lastName) ||
                other.lastName == lastName) &&
            (identical(other.usaBoxingId, usaBoxingId) ||
                other.usaBoxingId == usaBoxingId) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.weight, weight) || other.weight == weight) &&
            (identical(other.dateOfBirth, dateOfBirth) ||
                other.dateOfBirth == dateOfBirth) &&
            (identical(other.numberOfFights, numberOfFights) ||
                other.numberOfFights == numberOfFights) &&
            (identical(other.teamId, teamId) || other.teamId == teamId) &&
            (identical(other.gymName, gymName) || other.gymName == gymName) &&
            (identical(other.pictureThumb, pictureThumb) ||
                other.pictureThumb == pictureThumb) &&
            (identical(other.isBoxer, isBoxer) || other.isBoxer == isBoxer) &&
            (identical(other.isCoach, isCoach) || other.isCoach == isCoach) &&
            (identical(other.isOfficial, isOfficial) ||
                other.isOfficial == isOfficial) &&
            (identical(other.isMatchmaker, isMatchmaker) ||
                other.isMatchmaker == isMatchmaker) &&
            (identical(other.isManagedAccount, isManagedAccount) ||
                other.isManagedAccount == isManagedAccount) &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.age, age) || other.age == age) &&
            (identical(other.gymLocation, gymLocation) ||
                other.gymLocation == gymLocation));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      firstName,
      lastName,
      usaBoxingId,
      gender,
      weight,
      dateOfBirth,
      numberOfFights,
      teamId,
      gymName,
      pictureThumb,
      isBoxer,
      isCoach,
      isOfficial,
      isMatchmaker,
      isManagedAccount,
      id,
      age,
      gymLocation);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FighterInfoImplCopyWith<_$FighterInfoImpl> get copyWith =>
      __$$FighterInfoImplCopyWithImpl<_$FighterInfoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$FighterInfoImplToJson(
      this,
    );
  }
}

abstract class _FighterInfo extends FighterInfo {
  const factory _FighterInfo(
          {@JsonKey(name: 'first_name') final String firstName,
          @JsonKey(name: 'last_name') final String lastName,
          @JsonKey(name: 'usa_boxing_id') final String? usaBoxingId,
          final String gender,
          final String weight,
          @JsonKey(name: 'date_of_birth') final DateTime? dateOfBirth,
          @JsonKey(name: 'number_of_fights') final int? numberOfFights,
          @JsonKey(name: 'team_id') final int? teamId,
          @JsonKey(name: 'gym_name') final String? gymName,
          @JsonKey(name: 'picture_thumb') final String? pictureThumb,
          @JsonKey(name: 'is_boxer') final bool? isBoxer,
          @JsonKey(name: 'is_coach') final bool? isCoach,
          @JsonKey(name: 'is_official') final bool? isOfficial,
          @JsonKey(name: 'is_matchmaker') final bool? isMatchmaker,
          @JsonKey(name: 'is_managed_account') final bool? isManagedAccount,
          final int? id,
          final int age,
          @JsonKey(name: 'gym_location_full') final GymLocation? gymLocation}) =
      _$FighterInfoImpl;
  const _FighterInfo._() : super._();

  factory _FighterInfo.fromJson(Map<String, dynamic> json) =
      _$FighterInfoImpl.fromJson;

  @override
  @JsonKey(name: 'first_name')
  String get firstName;
  @override
  @JsonKey(name: 'last_name')
  String get lastName;
  @override
  @JsonKey(name: 'usa_boxing_id')
  String? get usaBoxingId;
  @override
  String get gender;
  @override
  String get weight;
  @override
  @JsonKey(name: 'date_of_birth')
  DateTime? get dateOfBirth;
  @override
  @JsonKey(name: 'number_of_fights')
  int? get numberOfFights;
  @override
  @JsonKey(name: 'team_id')
  int? get teamId;
  @override
  @JsonKey(name: 'gym_name')
  String? get gymName;
  @override
  @JsonKey(name: 'picture_thumb')
  String? get pictureThumb;
  @override
  @JsonKey(name: 'is_boxer')
  bool? get isBoxer;
  @override
  @JsonKey(name: 'is_coach')
  bool? get isCoach;
  @override
  @JsonKey(name: 'is_official')
  bool? get isOfficial;
  @override
  @JsonKey(name: 'is_matchmaker')
  bool? get isMatchmaker;
  @override
  @JsonKey(name: 'is_managed_account')
  bool? get isManagedAccount;
  @override
  int? get id;
  @override
  int get age;
  @override
  @JsonKey(name: 'gym_location_full')
  GymLocation? get gymLocation;
  @override
  @JsonKey(ignore: true)
  _$$FighterInfoImplCopyWith<_$FighterInfoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

GymLocation _$GymLocationFromJson(Map<String, dynamic> json) {
  return _GymLocation.fromJson(json);
}

/// @nodoc
mixin _$GymLocation {
  String? get locality => throw _privateConstructorUsedError;
  String? get state => throw _privateConstructorUsedError;
  @JsonKey(name: 'state_code')
  String? get stateCode => throw _privateConstructorUsedError;
  String? get country => throw _privateConstructorUsedError;
  @JsonKey(name: 'country_code')
  String? get countryCode => throw _privateConstructorUsedError;
  String? get raw => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GymLocationCopyWith<GymLocation> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GymLocationCopyWith<$Res> {
  factory $GymLocationCopyWith(
          GymLocation value, $Res Function(GymLocation) then) =
      _$GymLocationCopyWithImpl<$Res, GymLocation>;
  @useResult
  $Res call(
      {String? locality,
      String? state,
      @JsonKey(name: 'state_code') String? stateCode,
      String? country,
      @JsonKey(name: 'country_code') String? countryCode,
      String? raw});
}

/// @nodoc
class _$GymLocationCopyWithImpl<$Res, $Val extends GymLocation>
    implements $GymLocationCopyWith<$Res> {
  _$GymLocationCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? locality = freezed,
    Object? state = freezed,
    Object? stateCode = freezed,
    Object? country = freezed,
    Object? countryCode = freezed,
    Object? raw = freezed,
  }) {
    return _then(_value.copyWith(
      locality: freezed == locality
          ? _value.locality
          : locality // ignore: cast_nullable_to_non_nullable
              as String?,
      state: freezed == state
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as String?,
      stateCode: freezed == stateCode
          ? _value.stateCode
          : stateCode // ignore: cast_nullable_to_non_nullable
              as String?,
      country: freezed == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      countryCode: freezed == countryCode
          ? _value.countryCode
          : countryCode // ignore: cast_nullable_to_non_nullable
              as String?,
      raw: freezed == raw
          ? _value.raw
          : raw // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GymLocationImplCopyWith<$Res>
    implements $GymLocationCopyWith<$Res> {
  factory _$$GymLocationImplCopyWith(
          _$GymLocationImpl value, $Res Function(_$GymLocationImpl) then) =
      __$$GymLocationImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? locality,
      String? state,
      @JsonKey(name: 'state_code') String? stateCode,
      String? country,
      @JsonKey(name: 'country_code') String? countryCode,
      String? raw});
}

/// @nodoc
class __$$GymLocationImplCopyWithImpl<$Res>
    extends _$GymLocationCopyWithImpl<$Res, _$GymLocationImpl>
    implements _$$GymLocationImplCopyWith<$Res> {
  __$$GymLocationImplCopyWithImpl(
      _$GymLocationImpl _value, $Res Function(_$GymLocationImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? locality = freezed,
    Object? state = freezed,
    Object? stateCode = freezed,
    Object? country = freezed,
    Object? countryCode = freezed,
    Object? raw = freezed,
  }) {
    return _then(_$GymLocationImpl(
      locality: freezed == locality
          ? _value.locality
          : locality // ignore: cast_nullable_to_non_nullable
              as String?,
      state: freezed == state
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as String?,
      stateCode: freezed == stateCode
          ? _value.stateCode
          : stateCode // ignore: cast_nullable_to_non_nullable
              as String?,
      country: freezed == country
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      countryCode: freezed == countryCode
          ? _value.countryCode
          : countryCode // ignore: cast_nullable_to_non_nullable
              as String?,
      raw: freezed == raw
          ? _value.raw
          : raw // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GymLocationImpl extends _GymLocation {
  const _$GymLocationImpl(
      {this.locality,
      this.state,
      @JsonKey(name: 'state_code') this.stateCode,
      this.country,
      @JsonKey(name: 'country_code') this.countryCode,
      this.raw})
      : super._();

  factory _$GymLocationImpl.fromJson(Map<String, dynamic> json) =>
      _$$GymLocationImplFromJson(json);

  @override
  final String? locality;
  @override
  final String? state;
  @override
  @JsonKey(name: 'state_code')
  final String? stateCode;
  @override
  final String? country;
  @override
  @JsonKey(name: 'country_code')
  final String? countryCode;
  @override
  final String? raw;

  @override
  String toString() {
    return 'GymLocation(locality: $locality, state: $state, stateCode: $stateCode, country: $country, countryCode: $countryCode, raw: $raw)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GymLocationImpl &&
            (identical(other.locality, locality) ||
                other.locality == locality) &&
            (identical(other.state, state) || other.state == state) &&
            (identical(other.stateCode, stateCode) ||
                other.stateCode == stateCode) &&
            (identical(other.country, country) || other.country == country) &&
            (identical(other.countryCode, countryCode) ||
                other.countryCode == countryCode) &&
            (identical(other.raw, raw) || other.raw == raw));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, locality, state, stateCode, country, countryCode, raw);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GymLocationImplCopyWith<_$GymLocationImpl> get copyWith =>
      __$$GymLocationImplCopyWithImpl<_$GymLocationImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GymLocationImplToJson(
      this,
    );
  }
}

abstract class _GymLocation extends GymLocation {
  const factory _GymLocation(
      {final String? locality,
      final String? state,
      @JsonKey(name: 'state_code') final String? stateCode,
      final String? country,
      @JsonKey(name: 'country_code') final String? countryCode,
      final String? raw}) = _$GymLocationImpl;
  const _GymLocation._() : super._();

  factory _GymLocation.fromJson(Map<String, dynamic> json) =
      _$GymLocationImpl.fromJson;

  @override
  String? get locality;
  @override
  String? get state;
  @override
  @JsonKey(name: 'state_code')
  String? get stateCode;
  @override
  String? get country;
  @override
  @JsonKey(name: 'country_code')
  String? get countryCode;
  @override
  String? get raw;
  @override
  @JsonKey(ignore: true)
  _$$GymLocationImplCopyWith<_$GymLocationImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
