// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fighter_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$FighterInfoImpl _$$FighterInfoImplFromJson(Map<String, dynamic> json) =>
    _$FighterInfoImpl(
      firstName: json['first_name'] as String? ?? 'NA',
      lastName: json['last_name'] as String? ?? 'NA',
      usaBoxingId: json['usa_boxing_id'] as String?,
      gender: json['gender'] as String? ?? 'NA',
      weight: json['weight'] as String? ?? '0',
      dateOfBirth: json['date_of_birth'] == null
          ? null
          : DateTime.parse(json['date_of_birth'] as String),
      numberOfFights: json['number_of_fights'] as int?,
      teamId: json['team_id'] as int?,
      gymName: json['gym_name'] as String?,
      pictureThumb: json['picture_thumb'] as String?,
      isBoxer: json['is_boxer'] as bool?,
      isCoach: json['is_coach'] as bool?,
      isOfficial: json['is_official'] as bool?,
      isMatchmaker: json['is_matchmaker'] as bool?,
      isManagedAccount: json['is_managed_account'] as bool?,
      id: json['id'] as int?,
      age: json['age'] as int? ?? 0,
      gymLocation: json['gym_location_full'] == null
          ? null
          : GymLocation.fromJson(
              json['gym_location_full'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$FighterInfoImplToJson(_$FighterInfoImpl instance) =>
    <String, dynamic>{
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'usa_boxing_id': instance.usaBoxingId,
      'gender': instance.gender,
      'weight': instance.weight,
      'date_of_birth': instance.dateOfBirth?.toIso8601String(),
      'number_of_fights': instance.numberOfFights,
      'team_id': instance.teamId,
      'gym_name': instance.gymName,
      'picture_thumb': instance.pictureThumb,
      'is_boxer': instance.isBoxer,
      'is_coach': instance.isCoach,
      'is_official': instance.isOfficial,
      'is_matchmaker': instance.isMatchmaker,
      'is_managed_account': instance.isManagedAccount,
      'id': instance.id,
      'age': instance.age,
      'gym_location_full': instance.gymLocation,
    };

_$GymLocationImpl _$$GymLocationImplFromJson(Map<String, dynamic> json) =>
    _$GymLocationImpl(
      locality: json['locality'] as String?,
      state: json['state'] as String?,
      stateCode: json['state_code'] as String?,
      country: json['country'] as String?,
      countryCode: json['country_code'] as String?,
      raw: json['raw'] as String?,
    );

Map<String, dynamic> _$$GymLocationImplToJson(_$GymLocationImpl instance) =>
    <String, dynamic>{
      'locality': instance.locality,
      'state': instance.state,
      'state_code': instance.stateCode,
      'country': instance.country,
      'country_code': instance.countryCode,
      'raw': instance.raw,
    };
