import 'package:async/async.dart';
import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:konquer_test/data/dto/fighter_info.dart';
import 'package:konquer_test/data/dto/paginated_data.dart';
import 'package:konquer_test/data/http_client_provider.dart';

/// Provider for [RosterRepository]
final rosterRepositoryProvider = Provider(
  (ref) => RosterRepository(
    dioClient: ref.watch(httpClientProvider),
  ),
);

class RosterRepository {
  final Dio _dioClient;

  RosterRepository({required Dio dioClient}) : _dioClient = dioClient;

  Future<Result<PaginatedData<FighterInfo>>> getFightersList({
    String? lowerAgeLimit,
    String? upperAgeLimit,
    String? nameSearch,
    int page = 1,
  }) async {
    try {
      var response = await _dioClient.get('users', queryParameters: {
        'page': page,
        if (lowerAgeLimit?.isNotEmpty ?? false) 'age__gte': lowerAgeLimit,
        if (upperAgeLimit?.isNotEmpty ?? false) 'age__lte': upperAgeLimit,
        if (nameSearch?.isNotEmpty ?? false) 'name': nameSearch,
      });

      if (response.statusCode == 200) {
        return Result.value(
          PaginatedData.fromJson(
            (response.data as Map<String, dynamic>)..addAll({'page': page}),
            FighterInfo.fromJson,
          ),
        );
      }

      return Result.error(response.data.toString());
    } catch (error, trace) {
      return Result.error(error.toString(), trace);
    }
  }
}
