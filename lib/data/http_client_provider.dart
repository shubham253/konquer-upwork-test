import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final httpClientProvider = Provider((ref) {
  /// Base HTTP headers used with all client calls.
  const Map<String, String> baseHeaders = <String, String>{
    HttpHeaders.acceptHeader: '*/*',
    HttpHeaders.contentTypeHeader: 'application/json; charset=UTF-8',
  };
  final client = Dio(
    BaseOptions(
      validateStatus: (_) => true,
      baseUrl: 'https://api.konquer.club/api/v2/',
      headers: <String, String>{
        ...baseHeaders,
        HttpHeaders.authorizationHeader:
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNj'
                'ZXNzIiwiZXhwIjoxNzA4MTU0ODc4LCJpYXQiOjE2NzY2MTg4NzgsImp0aSI6Ijc5Yj'
                'Y3YTZlN2FlZjQzZTA5ZDAzY2FjYTdhODlhNDAyIiwidXNlcl9pZCI6NTgzMX0.q98'
                'N_5jym6uCY9_lgn6xK9qzxw2jSI57Is8pb9uosLk',
      },
    ),
  );

  return client;
});
