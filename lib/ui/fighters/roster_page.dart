import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:konquer_test/state/roster_page_manager.dart';
import 'package:konquer_test/ui/fighters/fighters_list.dart';

class RosterPage extends ConsumerStatefulWidget {
  const RosterPage({super.key});

  @override
  ConsumerState<RosterPage> createState() => _RosterPageState();
}

class _RosterPageState extends ConsumerState<RosterPage>
    with SingleTickerProviderStateMixin {
  late final TabController _controller = TabController(
    vsync: this,
    length: tabs.length,
    initialIndex: 0,
  );

  final List<Tab> tabs = <Tab>[
    const Tab(text: '0 - 35'),
    const Tab(text: '35 - 99'),
  ];

  final TextEditingController _searchController = TextEditingController();
  bool _isSearching = false;

  @override
  Widget build(BuildContext context) {
    final manager = ref.read(rosterPageManagerProvider.notifier);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: _isSearching
            ? TextField(
                controller: _searchController,
                decoration: const InputDecoration(
                  hintText: 'Search...',
                  border: InputBorder.none,
                  hintStyle: TextStyle(color: Colors.white),
                ),
                style: const TextStyle(color: Colors.white),
                autofocus: true,
                onSubmitted: (_) =>
                    manager.executeSearch(_searchController.text),
              )
            : const Text('Roster'),
        actions: [
          _isSearching
              ? IconButton(
                  icon: const Icon(Icons.clear),
                  onPressed: () {
                    manager.clearSearch();
                    _searchController.clear();
                    setState(
                      () {
                        _isSearching = false;
                      },
                    );
                  },
                )
              : IconButton(
                  icon: const Icon(Icons.search),
                  onPressed: () {
                    setState(
                      () {
                        _isSearching = true;
                      },
                    );
                  },
                ),
        ],
        bottom: TabBar(
          controller: _controller,
          tabs: tabs,
        ),
      ),
      body: TabBarView(
        controller: _controller,
        children: const [
          FightersList('0,35'),
          FightersList('35,99'),
        ],
      ),
    );
  }
}
