import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:konquer_test/state/fighter_list_manager.dart';
import 'package:konquer_test/ui/fighters/fighter_list_item.dart';

class FightersList extends ConsumerStatefulWidget {
  // Lower and upper age limit separated by comma
  final String ageLimit;

  const FightersList(
    this.ageLimit, {
    super.key,
  });

  @override
  ConsumerState<FightersList> createState() => _FightersListState();
}

class _FightersListState extends ConsumerState<FightersList> {
  @override
  Widget build(BuildContext context) {
    final provider = fighterListManagerProvider(widget.ageLimit);
    final state = ref.watch(provider);
    final mainList = state.mainFighterList;
    return NotificationListener(
      onNotification: (notification) {
        if (notification is ScrollEndNotification &&
            notification.metrics.extentAfter == 0) {
          // End of list reached
          ref.read(provider.notifier).refreshData(fetchNext: true);
        }
        return false;
      },
      child: ListView.separated(
        itemBuilder: (ctx, index) {
          if (index >= mainList.length) {
            return state.lastFetchedList is AsyncLoading
                ? const Center(child: CircularProgressIndicator())
                : Container(height: 50);
          }
          final item = mainList[index];
          return FighterListItem(item: item);
        },
        separatorBuilder: (ctx, index) => Container(
          height: 2,
          // color: Colors.black,
        ),
        itemCount: mainList.length + 1,
      ),
    );
  }
}
