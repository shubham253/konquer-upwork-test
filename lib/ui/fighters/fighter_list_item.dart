import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:konquer_test/data/dto/fighter_info.dart';

class FighterListItem extends StatelessWidget {
  final FighterInfo item;

  const FighterListItem({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            (item.pictureThumb?.isEmpty ?? true)
                ? const Icon(Icons.person)
                : SvgPicture.network(
                    item.pictureThumb!,
                    height: 50,
                    width: 50,
                    placeholderBuilder: (BuildContext context) => const Icon(
                      Icons.person,
                      size: 50,
                    ),
                  ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0),
              child: Row(
                children: [
                  const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('ID :'),
                      Text('First Name :'),
                      Text('Last Name :'),
                      Text('Gender :'),
                      Text('Age :'),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(item.id?.toString() ?? '_'),
                        Text(item.firstName),
                        Text(item.lastName),
                        Text(item.gender),
                        Text(item.age.toString()),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
