//
// Copyright (c) 2022 NECS, Inc.
//

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

/// Helper widget to handle [AsyncValue] loading and error state.
/// Use this to handle data state
class AsyncValueView<T> extends ConsumerWidget {
  /// Default constructor
  const AsyncValueView(
      {Key? key, required this.data, required this.onSuccess, this.height})
      : super(key: key);

  /// It will be used to create view for different state
  final AsyncValue<T> data;

  /// Callback function to return view in case of proper data
  final Widget Function(T data) onSuccess;

  /// Optional height of error and loading view. By default it will shown in
  /// center of available space.
  final double? height;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return data.when(
      loading: () => SizedBox(
        height: height,
        child: const Center(child: CircularProgressIndicator()),
      ),
      error: (err, stack) => SizedBox(
        height: height,
        child: Center(
          child: Text(
            err.toString(),
          ),
        ),
      ),
      data: (data) {
        return onSuccess(data);
      },
    );
  }
}
