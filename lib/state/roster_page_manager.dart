import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'roster_page_manager.freezed.dart';

final rosterPageManagerProvider =
    StateNotifierProvider<RosterPageStateManager, RosterPageState>(
  (ref) => RosterPageStateManager(
    RosterPageState(),
  ),
);

@freezed
class RosterPageState with _$RosterPageState {
  factory RosterPageState({
    @Default('') String searchKey,
  }) = _RosterPageState;
}

class RosterPageStateManager extends StateNotifier<RosterPageState> {
  RosterPageStateManager(super.state);

  clearSearch() {
    state = state.copyWith(searchKey: '');
  }

  executeSearch(String searchKey) {
    state = state.copyWith(searchKey: searchKey);
  }
}
