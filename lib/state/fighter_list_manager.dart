import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:konquer_test/data/dto/fighter_info.dart';
import 'package:konquer_test/data/dto/paginated_data.dart';
import 'package:konquer_test/data/repository/roster_repository.dart';
import 'package:konquer_test/state/roster_page_manager.dart';

part 'fighter_list_manager.freezed.dart';

final fighterListManagerProvider = StateNotifierProvider.family<
    FighterListStateManager, FighterListState, String>(
  (ref, ageLimit) => FighterListStateManager(
    FighterListState(),
    ageLimit.split(',')[0],
    ageLimit.split(',')[1],
    ref,
  ),
);

@freezed
class FighterListState with _$FighterListState {
  factory FighterListState({
    @Default(
      <FighterInfo>[],
    )
    List<FighterInfo> mainFighterList,
    @Default(
      AsyncData(null),
    )
    AsyncValue<PaginatedData<FighterInfo>?> lastFetchedList,
    @Default(0) int itemCount,
  }) = _FighterListState;
}

class FighterListStateManager extends StateNotifier<FighterListState> {
  final String lowerAgeLimit, upperAgeLimit;
  final Ref ref;
  late final ProviderSubscription? _providerSubscription;

  FighterListStateManager(
      super.state, this.lowerAgeLimit, this.upperAgeLimit, this.ref) {
    refreshData();
    _providerSubscription =
        ref.listen(rosterPageManagerProvider, (previous, next) {
      refreshData();
    });
  }

  refreshData({fetchNext = false}) async {
    var nextPage = state.lastFetchedList
            .mapOrNull(data: (data) => data.asData!.value?.page) ??
        1;
    if (fetchNext) {
      await Future.delayed(const Duration(milliseconds: 500));
      if (state.mainFighterList.isNotEmpty &&
          state.mainFighterList.length <= state.itemCount) {
        nextPage++;
      }
    }
    state = state.copyWith(lastFetchedList: const AsyncLoading());
    final result = await ref.read(rosterRepositoryProvider).getFightersList(
          lowerAgeLimit: lowerAgeLimit,
          upperAgeLimit: upperAgeLimit,
          nameSearch: ref.read(rosterPageManagerProvider).searchKey,
          page: nextPage,
        );
    if (result.isValue) {
      final valueResult = result.asValue!.value;
      state = state.copyWith(
          lastFetchedList: AsyncData(valueResult),
          mainFighterList: [
            if (fetchNext) ...state.mainFighterList,
            ...valueResult.items!
          ],
          itemCount: valueResult.count ?? -1);
      return true;
    } else {
      state = state.copyWith(
          lastFetchedList:
              AsyncError(result.asError!.error, result.asError!.stackTrace));
      return false;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _providerSubscription?.close();
  }
}
