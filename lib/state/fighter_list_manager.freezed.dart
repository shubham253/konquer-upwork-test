// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'fighter_list_manager.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FighterListState {
  List<FighterInfo> get mainFighterList => throw _privateConstructorUsedError;
  AsyncValue<PaginatedData<FighterInfo>?> get lastFetchedList =>
      throw _privateConstructorUsedError;
  int get itemCount => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FighterListStateCopyWith<FighterListState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FighterListStateCopyWith<$Res> {
  factory $FighterListStateCopyWith(
          FighterListState value, $Res Function(FighterListState) then) =
      _$FighterListStateCopyWithImpl<$Res, FighterListState>;
  @useResult
  $Res call(
      {List<FighterInfo> mainFighterList,
      AsyncValue<PaginatedData<FighterInfo>?> lastFetchedList,
      int itemCount});
}

/// @nodoc
class _$FighterListStateCopyWithImpl<$Res, $Val extends FighterListState>
    implements $FighterListStateCopyWith<$Res> {
  _$FighterListStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? mainFighterList = null,
    Object? lastFetchedList = null,
    Object? itemCount = null,
  }) {
    return _then(_value.copyWith(
      mainFighterList: null == mainFighterList
          ? _value.mainFighterList
          : mainFighterList // ignore: cast_nullable_to_non_nullable
              as List<FighterInfo>,
      lastFetchedList: null == lastFetchedList
          ? _value.lastFetchedList
          : lastFetchedList // ignore: cast_nullable_to_non_nullable
              as AsyncValue<PaginatedData<FighterInfo>?>,
      itemCount: null == itemCount
          ? _value.itemCount
          : itemCount // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FighterListStateImplCopyWith<$Res>
    implements $FighterListStateCopyWith<$Res> {
  factory _$$FighterListStateImplCopyWith(_$FighterListStateImpl value,
          $Res Function(_$FighterListStateImpl) then) =
      __$$FighterListStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<FighterInfo> mainFighterList,
      AsyncValue<PaginatedData<FighterInfo>?> lastFetchedList,
      int itemCount});
}

/// @nodoc
class __$$FighterListStateImplCopyWithImpl<$Res>
    extends _$FighterListStateCopyWithImpl<$Res, _$FighterListStateImpl>
    implements _$$FighterListStateImplCopyWith<$Res> {
  __$$FighterListStateImplCopyWithImpl(_$FighterListStateImpl _value,
      $Res Function(_$FighterListStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? mainFighterList = null,
    Object? lastFetchedList = null,
    Object? itemCount = null,
  }) {
    return _then(_$FighterListStateImpl(
      mainFighterList: null == mainFighterList
          ? _value._mainFighterList
          : mainFighterList // ignore: cast_nullable_to_non_nullable
              as List<FighterInfo>,
      lastFetchedList: null == lastFetchedList
          ? _value.lastFetchedList
          : lastFetchedList // ignore: cast_nullable_to_non_nullable
              as AsyncValue<PaginatedData<FighterInfo>?>,
      itemCount: null == itemCount
          ? _value.itemCount
          : itemCount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$FighterListStateImpl implements _FighterListState {
  _$FighterListStateImpl(
      {final List<FighterInfo> mainFighterList = const <FighterInfo>[],
      this.lastFetchedList = const AsyncData(null),
      this.itemCount = 0})
      : _mainFighterList = mainFighterList;

  final List<FighterInfo> _mainFighterList;
  @override
  @JsonKey()
  List<FighterInfo> get mainFighterList {
    if (_mainFighterList is EqualUnmodifiableListView) return _mainFighterList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_mainFighterList);
  }

  @override
  @JsonKey()
  final AsyncValue<PaginatedData<FighterInfo>?> lastFetchedList;
  @override
  @JsonKey()
  final int itemCount;

  @override
  String toString() {
    return 'FighterListState(mainFighterList: $mainFighterList, lastFetchedList: $lastFetchedList, itemCount: $itemCount)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FighterListStateImpl &&
            const DeepCollectionEquality()
                .equals(other._mainFighterList, _mainFighterList) &&
            (identical(other.lastFetchedList, lastFetchedList) ||
                other.lastFetchedList == lastFetchedList) &&
            (identical(other.itemCount, itemCount) ||
                other.itemCount == itemCount));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_mainFighterList),
      lastFetchedList,
      itemCount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FighterListStateImplCopyWith<_$FighterListStateImpl> get copyWith =>
      __$$FighterListStateImplCopyWithImpl<_$FighterListStateImpl>(
          this, _$identity);
}

abstract class _FighterListState implements FighterListState {
  factory _FighterListState(
      {final List<FighterInfo> mainFighterList,
      final AsyncValue<PaginatedData<FighterInfo>?> lastFetchedList,
      final int itemCount}) = _$FighterListStateImpl;

  @override
  List<FighterInfo> get mainFighterList;
  @override
  AsyncValue<PaginatedData<FighterInfo>?> get lastFetchedList;
  @override
  int get itemCount;
  @override
  @JsonKey(ignore: true)
  _$$FighterListStateImplCopyWith<_$FighterListStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
