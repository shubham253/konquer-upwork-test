// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'roster_page_manager.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RosterPageState {
  String get searchKey => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RosterPageStateCopyWith<RosterPageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RosterPageStateCopyWith<$Res> {
  factory $RosterPageStateCopyWith(
          RosterPageState value, $Res Function(RosterPageState) then) =
      _$RosterPageStateCopyWithImpl<$Res, RosterPageState>;
  @useResult
  $Res call({String searchKey});
}

/// @nodoc
class _$RosterPageStateCopyWithImpl<$Res, $Val extends RosterPageState>
    implements $RosterPageStateCopyWith<$Res> {
  _$RosterPageStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? searchKey = null,
  }) {
    return _then(_value.copyWith(
      searchKey: null == searchKey
          ? _value.searchKey
          : searchKey // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RosterPageStateImplCopyWith<$Res>
    implements $RosterPageStateCopyWith<$Res> {
  factory _$$RosterPageStateImplCopyWith(_$RosterPageStateImpl value,
          $Res Function(_$RosterPageStateImpl) then) =
      __$$RosterPageStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String searchKey});
}

/// @nodoc
class __$$RosterPageStateImplCopyWithImpl<$Res>
    extends _$RosterPageStateCopyWithImpl<$Res, _$RosterPageStateImpl>
    implements _$$RosterPageStateImplCopyWith<$Res> {
  __$$RosterPageStateImplCopyWithImpl(
      _$RosterPageStateImpl _value, $Res Function(_$RosterPageStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? searchKey = null,
  }) {
    return _then(_$RosterPageStateImpl(
      searchKey: null == searchKey
          ? _value.searchKey
          : searchKey // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$RosterPageStateImpl implements _RosterPageState {
  _$RosterPageStateImpl({this.searchKey = ''});

  @override
  @JsonKey()
  final String searchKey;

  @override
  String toString() {
    return 'RosterPageState(searchKey: $searchKey)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RosterPageStateImpl &&
            (identical(other.searchKey, searchKey) ||
                other.searchKey == searchKey));
  }

  @override
  int get hashCode => Object.hash(runtimeType, searchKey);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RosterPageStateImplCopyWith<_$RosterPageStateImpl> get copyWith =>
      __$$RosterPageStateImplCopyWithImpl<_$RosterPageStateImpl>(
          this, _$identity);
}

abstract class _RosterPageState implements RosterPageState {
  factory _RosterPageState({final String searchKey}) = _$RosterPageStateImpl;

  @override
  String get searchKey;
  @override
  @JsonKey(ignore: true)
  _$$RosterPageStateImplCopyWith<_$RosterPageStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
